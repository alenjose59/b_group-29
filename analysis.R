#loading required libraries
library(readr)
#loading the dataset
df <- read_csv("Mental_health_Depression_disorder_Data.csv")
#assigning variables
x<-df$`Anxiety disorders (%)`
y<-df$`Depression (%)`
#performing spearman analysis
cor.test(x,y,method="spearman")
